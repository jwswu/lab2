/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author wswu
 *
 */
public abstract class Temperature 
{
	private float value;
	public Temperature(float v)
	{
		value = v;
	}
	public final float getValue()
	{
		return value;
	}
	public abstract Temperature toCelsius();
	public abstract Temperature toFahrenheit();

}
